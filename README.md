
# devops-netology

**/.terraform/* - локальные директории будут проигнорированы.

*.tfstate - файлы типа .tfstate будут проигнорированы.

crash.log - игнорирование логов ошибок.

*.tfvars - игнорирование секретных ключей и паролей.

override.tf
override.tf.json
*_override.tf
*_override.tf.json - игнорирование файлов, которые переопределяют формат.

.terraformrc
terraform.rc  - игнорирование конфигурационных файлов.


